package project.sabs.android.com.playgame.SportsDetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManageSports.BatchTimeModel;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SportsBatchTimingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<BatchTimeModel> modelList;
    public static SportsBatchTimingListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    BatchTimeModel gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public SportsBatchTimingListAdapter(Context context, ArrayList<BatchTimeModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<BatchTimeModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_batch_timing_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final BatchTimeModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;
            genericViewHolder.txt_timing.setText(modelList.get(position).getStartTime()+"-"+modelList.get(position).getEndTime());


        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private BatchTimeModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SportsBatchTimingListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView txt_timing;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);


            this.txt_timing = (TextView) itemView.findViewById(R.id.txt_timing);


        }

    }

}

