package project.sabs.android.com.playgame.SportsDetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SportsCoachListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<StudentModel> modelList;
    public static SportsCoachListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public SportsCoachListAdapter(Context context, ArrayList<StudentModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<StudentModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_coach_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final StudentModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.CoachimageView.setImageResource(model.getUserImage());

            genericViewHolder.txtCoachName.setText(model.getSName());

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private StudentModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SportsCoachListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView CoachimageView;
        private TextView txtCoachName;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.CoachimageView = (ImageView) itemView.findViewById(R.id.CoachimageView);
            this.txtCoachName = (TextView) itemView.findViewById(R.id.txtCoachName);


        }

    }

}

