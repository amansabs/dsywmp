package project.sabs.android.com.playgame.Player;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManagePlayer.ManagePlayerActivity;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class StudentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<StudentModel> modelList;
    public static StudentListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public StudentListAdapter(Context context, ArrayList<StudentModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<StudentModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_student_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final StudentModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.UserimageView.setImageResource(model.getUserImage());

            genericViewHolder.txt_name.setText(model.getSName());
            genericViewHolder.txt_EmailId.setText(model.getSENROLL());
            genericViewHolder.txt_gameName.setText(model.getSGame());
            genericViewHolder.txt_timing.setText(model.getSTiming());
            genericViewHolder.txt_addDetail.setText(model.getSMemberShip());
            genericViewHolder.txt_MobileNumber.setText(model.getSMobile());
            genericViewHolder.call_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String dail = model.getSMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + dail));
                    mContext.startActivity(intent);

                }
            });
            //setAnimation(holder.itemView, position);

            genericViewHolder.edit_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(mContext, ManagePlayerActivity.class);
                    mContext.startActivity(in);
                }
            });

            genericViewHolder.rr_playerView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.logout_dialog);
                    Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                    Button logout_btn=dialog.findViewById(R.id.logout_button);
                    TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                    TextView txt_subject=dialog.findViewById(R.id.txt_subject);
                    txt_heading.setText("Delete Player");
                    logout_btn.setText("DELETE");
                    txt_subject.setText("Are you sure you want to delete this player ?");
                    cancel_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });



                    logout_btn.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            modelList.remove(position);
                            notifyDataSetChanged();
                            dialog.dismiss();

                        }
                    });

                    return true;
                }
            });

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private StudentModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, StudentListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView UserimageView;
        private ImageView call_img;
        private ImageView edit_img;
        private TextView txt_name;
        private TextView txt_EmailId;
        private TextView txt_gameName;
        private TextView txt_timing;
        private TextView txt_addDetail;
        private TextView txt_MobileNumber;
        private RelativeLayout rr_playerView;
        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.UserimageView = (ImageView) itemView.findViewById(R.id.UserimageView);
            this.call_img = (ImageView) itemView.findViewById(R.id.call_img);
            this.edit_img = (ImageView) itemView.findViewById(R.id.edit_img);
            this.txt_name = (TextView) itemView.findViewById(R.id.txt_name);
            this.txt_EmailId = (TextView) itemView.findViewById(R.id.txt_EmailId);
            this.txt_gameName = (TextView) itemView.findViewById(R.id.txt_gameName);
            this.txt_timing = (TextView) itemView.findViewById(R.id.txt_timing);
            this.txt_addDetail = (TextView) itemView.findViewById(R.id.txt_addDetail);
            this.txt_MobileNumber = (TextView) itemView.findViewById(R.id.txt_MobileNumber);
            this.rr_playerView = (RelativeLayout) itemView.findViewById(R.id.rr_playerView);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

