package project.sabs.android.com.playgame.Register;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;

public class RegisterActivity extends AppCompatActivity {

    RelativeLayout rr_back;
    TextView text_toolbar;

    Spinner spinnerGame ;
    final ArrayList<GameModel> Game_nameList=new ArrayList<GameModel>();
    //ArrayList<GameModel> newCrop_nameList=new ArrayList<GameModel>();
    ArrayAdapter<GameModel> gameDataAdapter=null;
    String gameId="";


    Spinner spinnerTiming ;
    final ArrayList<TimingModel> timing_nameList=new ArrayList<TimingModel>();
    //ArrayList<TimingModel> newCrop_nameList=new ArrayList<TimingModel>();
    ArrayAdapter<TimingModel> timingDataAdapter=null;
    String timeId="";

    TextView txt_fees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        spinnerGame = (Spinner)findViewById(R.id.spinnerGame);
        spinnerTiming = (Spinner)findViewById(R.id.spinnerTiming);
        txt_fees = (TextView) findViewById(R.id.txt_fees);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        rr_back = (RelativeLayout)findViewById(R.id.rr_back);
        text_toolbar = (TextView)findViewById(R.id.text_toolbar);

        text_toolbar.setText("Student Registration");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        gameSelect();
        timingSelect();
    }


    private void gameSelect(){
        //spinnerCrop.setEnabled(true);
        Game_nameList.clear();
        Game_nameList.add(new GameModel("0", "0 Rs","Select Game"));
        Game_nameList.add(new GameModel("1", "500 Rs","Chess"));
        Game_nameList.add(new GameModel("2", "1000 Rs","Hockey"));
        Game_nameList.add(new GameModel("3", "2000 Rs","Cricket"));
        Game_nameList.add(new GameModel("4", "1500 Rs","Shooting"));

        gameDataAdapter = new ArrayAdapter<GameModel>(this,android.R.layout.simple_spinner_item, Game_nameList);
        gameDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGame.setAdapter(gameDataAdapter);

        spinnerGame.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Object country_item = parent.getItemAtPosition(pos);
                gameId ="";
                // subCropId="";


                gameId =Game_nameList.get(pos).getId();
                String fees = Game_nameList.get(pos).getFees();

                if(gameId != null && !gameId.equalsIgnoreCase("0") && !gameId.equalsIgnoreCase("")){
                    txt_fees.setText(fees);
                }
                else {
                    txt_fees.setText(fees);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void timingSelect(){
        //spinnerCrop.setEnabled(true);
        timing_nameList.clear();
        timing_nameList.clear();
        timing_nameList.add(new TimingModel("0","Select Timing"));
        timing_nameList.add(new TimingModel("1","10 AM to 11 AM"));
        timing_nameList.add(new TimingModel("2","09 AM to 10 AM"));
        timing_nameList.add(new TimingModel("3","12 PM to 01 PM"));
        timing_nameList.add(new TimingModel("4","01 PM to 02 PM"));

        timingDataAdapter = new ArrayAdapter<TimingModel>(this,android.R.layout.simple_spinner_item, timing_nameList);
        timingDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTiming.setAdapter(timingDataAdapter);

        spinnerTiming.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Object country_item = parent.getItemAtPosition(pos);
                timeId ="";
                // subCropId="";


                gameId =Game_nameList.get(pos).getId();
               // String ti = Game_nameList.get(pos).getFees();

                if(gameId != null && !gameId.equalsIgnoreCase("0") && !gameId.equalsIgnoreCase("")){
                    //txt_fees.setText(fees);
                }
                else {
                    //txt_fees.setText(fees);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


}
