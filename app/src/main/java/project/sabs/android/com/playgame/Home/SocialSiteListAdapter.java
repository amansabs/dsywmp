package project.sabs.android.com.playgame.Home;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SocialSiteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<SocialSite> modelList;
    public static SocialSiteListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public SocialSiteListAdapter(Context context, ArrayList<SocialSite> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<SocialSite> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_socialsite_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final SocialSite model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;


            genericViewHolder.txt_social_name.setText(model.getName());
            genericViewHolder.image_social.setImageResource(model.getImg());

            genericViewHolder.re_siteClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (position == 0){
                        Uri uri = Uri.parse("https://www.facebook.com/DSYWMPofficial/");
                        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                        likeIng.setPackage("com.facebook.android");
                        try {
                            mContext.startActivity(likeIng);
                        } catch (ActivityNotFoundException e) {
                            mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://facebook.com")));
                        }
                    }
                    if (position == 1){
                        Uri uri = Uri.parse("https://www.instagram.com/dsywmpofficial/");
                        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                        likeIng.setPackage("com.instagram.android");
                        try {
                            mContext.startActivity(likeIng);
                        } catch (ActivityNotFoundException e) {
                            mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://instagram.com")));
                        }
                    }
                    if (position == 2){
                        Uri uri = Uri.parse("https://twitter.com/dsywmpofficial");
                        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                        likeIng.setPackage("com.twitter.android");
                        try {
                            mContext.startActivity(likeIng);
                        } catch (ActivityNotFoundException e) {
                            mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://twitter.com")));
                        }
                    }
                }
            });


        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private SocialSite getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SocialSiteListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView image_social;
        private TextView txt_social_name;
        private RelativeLayout re_siteClick;


        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);
            this.image_social = (ImageView) itemView.findViewById(R.id.image_social);
            this.txt_social_name = (TextView) itemView.findViewById(R.id.txt_social_name);
            this.re_siteClick = (RelativeLayout) itemView.findViewById(R.id.re_siteClick);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

