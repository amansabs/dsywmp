package project.sabs.android.com.playgame.ManageUser;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Login.LoginActivity;
import project.sabs.android.com.playgame.MainActivity;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<UserModel> modelList;
    public static UserListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    RelativeLayout rr_noRecord;
    RecyclerView userlistRecyclerview;

    private OnItemClickListener mItemClickListener;


    public UserListAdapter(Context context, ArrayList<UserModel> modelList, RelativeLayout rr_noRecord, RecyclerView userlistRecyclerview) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.rr_noRecord = rr_noRecord;
        this.userlistRecyclerview = userlistRecyclerview;

    }


    public void updateList(ArrayList<UserModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_item_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final UserModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;





            genericViewHolder.UserimageView.setImageResource(model.getuImage());
            genericViewHolder.txt_name.setText(model.getuName());
            genericViewHolder.txt_departmentName.setText(model.getuDepartment());
            genericViewHolder.txt_mobile.setText(model.getuMobile());
            genericViewHolder.rr_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.logout_dialog);
                    Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                    Button logout_btn=dialog.findViewById(R.id.logout_button);
                    TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                    TextView txt_subject=dialog.findViewById(R.id.txt_subject);
                    txt_heading.setText("Delete User");
                    logout_btn.setText("Delete");
                    txt_subject.setText("Are you sure you want to delete this user ?");

                    cancel_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    logout_btn.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                           modelList.remove(position);
                           notifyDataSetChanged();
                           dialog.cancel();

                           if(modelList.size()== 0){
                               rr_noRecord.setVisibility(View.VISIBLE);
                               userlistRecyclerview.setVisibility(View.GONE);
                           }

                        }
                    });

                }
            });


            setAnimation(holder.itemView, position);

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private UserModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, UserListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView UserimageView;
        private TextView txt_name;
        private TextView txt_mobile;
        private TextView txt_departmentName;
        private RelativeLayout rr_delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.UserimageView = (ImageView) itemView.findViewById(R.id.UserimageView);
            this.txt_name = (TextView) itemView.findViewById(R.id.txt_name);
            this.txt_mobile = (TextView) itemView.findViewById(R.id.txt_mobile);
            this.txt_departmentName = (TextView) itemView.findViewById(R.id.txt_departmentName);
            this.rr_delete = (RelativeLayout) itemView.findViewById(R.id.rr_delete);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

