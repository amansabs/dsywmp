package project.sabs.android.com.playgame.ContactUs;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import project.sabs.android.com.playgame.R;

public class ContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    RelativeLayout rr_back;
    TextView text_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        rr_back = (RelativeLayout)findViewById(R.id.rr_back);

        text_toolbar = (TextView)findViewById(R.id.text_toolbar);

        text_toolbar.setText("Contact Us");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.

       /* googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.2314334, 77.3997975))
                .title("Tatya Tope Stadium Bhopal")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.2314334, 77.3997975), 15f));*/

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.1949301, 77.4690686))
                .title("Sarvoday Shopping Complex")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.1949301, 77.4690686), 15f));

    }
}
