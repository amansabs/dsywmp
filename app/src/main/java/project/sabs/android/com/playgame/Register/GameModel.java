package project.sabs.android.com.playgame.Register;

public class GameModel {

    String id;
    String fees;
    String Name;

    public GameModel(String id, String fees, String name) {
        this.id = id;
        this.fees = fees;
        Name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
