package project.sabs.android.com.playgame.SportsDetails;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManageSports.BatchTimeModel;
import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;

public class SportsDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View rootView;
    private SportsBatchTimingListAdapter sportsBatchTimingListAdapter;
    private SportsCoachListAdapter mAdapter;
    public ArrayList<StudentModel> StudentList =new ArrayList<StudentModel>();
    public ArrayList<BatchTimeModel> BatchList =new ArrayList<BatchTimeModel>();
    RecyclerView recycler_view_coach_list;
    RecyclerView recycler_batch_time_list;


    public SportsDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SportsDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SportsDetailFragment newInstance(String param1, String param2) {
        SportsDetailFragment fragment = new SportsDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_sports_detail, container, false);
        recycler_view_coach_list = (RecyclerView) rootView.findViewById(R.id.recycler_view_coach_list);
        recycler_batch_time_list = (RecyclerView) rootView.findViewById(R.id.recycler_batch_time_list);

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            /*Toast.makeText(getContext(), "setuservisible", Toast.LENGTH_SHORT).show();
            callBack();*/
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){
            BatchList();
            StudentList();
        }
    }
    private void StudentList()
    {
        StudentList.clear();

        StudentList.add(new StudentModel("Rahul Jain","7698989521","TT01523","Cricket","10 AM to 12 PM"
                ,"12 Nov to 30Dec 2019",R.drawable.user_one));
        StudentList.add(new StudentModel("Sachin Verma","7987673022","TT01522","Athlet","08 AM to 10 AM"
                ,"12 Oct to 12 Jan 2020",R.drawable.three_img));


        mAdapter = new SportsCoachListAdapter(getContext(), StudentList);
        recycler_view_coach_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_coach_list.setLayoutManager(layoutManager);
        recycler_view_coach_list.setNestedScrollingEnabled(false);
        recycler_view_coach_list.setAdapter(mAdapter);


    }
    private void BatchList()
    {
        BatchList.clear();
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));
        BatchList.add(new BatchTimeModel("8 Am ","9 AM",""));


        sportsBatchTimingListAdapter = new SportsBatchTimingListAdapter(getContext(), BatchList);
        recycler_batch_time_list.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        recycler_batch_time_list.setLayoutManager(layoutManager);
        recycler_batch_time_list.setNestedScrollingEnabled(false);
        recycler_batch_time_list.setAdapter(sportsBatchTimingListAdapter);


    }

}
