package project.sabs.android.com.playgame.Events;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;


public class EventListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View  rootView;
    private EventsListAdapter mAdapter;
    public ArrayList<EventModel> EventsList =new ArrayList<EventModel>();
    RecyclerView recycler_view_crop_list;


    public EventListFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EventListFragment newInstance(String param1, String param2) {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_event_list, container, false);
        recycler_view_crop_list= (RecyclerView) rootView.findViewById(R.id.recycler_view_crop_list);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            /*Toast.makeText(getContext(), "setuservisible", Toast.LENGTH_SHORT).show();
            callBack();*/
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){

            EventList();
        }
    }

    private void EventList()
    {
        EventsList.clear();
        EventsList.add(new EventModel("Bhopal Games (Men)Athletes","DEC 22-28,2019","Bhopal","TT Nagar Bhopal","Athletes",R.drawable.merathon_img));
        EventsList.add(new EventModel("National School Game Shooting Championship","DEC 25-28,2019","Bhopal","Chinar Park near ashima mall Bhopal","Shooting",R.drawable.shooting_img));
        EventsList.add(new EventModel("National School Game Hocky Championship","DEC 20-Jan 01,2020","Bhopal","TT Nagar Stadium Bhopal","Hocky",R.drawable.hocky_img));
        EventsList.add(new EventModel("Bhopal Games (Men)Athletes","DEC 22-28,2019","Bhopal","TT Nagar Bhopal","Athletes",R.drawable.merathon_img));
        EventsList.add(new EventModel("National School Game Shooting Championship","DEC 25-28,2019","Bhopal","Chinar Park near ashima mall Bhopal","Shooting",R.drawable.shooting_img));
        EventsList.add(new EventModel("National School Game Hocky Championship","DEC 20-Jan 01,2020","Bhopal","TT Nagar Stadium Bhopal","Hocky",R.drawable.hocky_img));


        mAdapter = new EventsListAdapter(getContext(), EventsList);
        recycler_view_crop_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_crop_list.setLayoutManager(layoutManager);
        recycler_view_crop_list.setAdapter(mAdapter);


    }


}
