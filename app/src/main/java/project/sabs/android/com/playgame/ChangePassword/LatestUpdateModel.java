package project.sabs.android.com.playgame.ChangePassword;

public class LatestUpdateModel {
    String latestUpdate;
    String date;

    public LatestUpdateModel(String latestUpdate, String date) {
        this.latestUpdate = latestUpdate;
        this.date = date;
    }

    public String getLatestUpdate() {
        return latestUpdate;
    }

    public void setLatestUpdate(String latestUpdate) {
        this.latestUpdate = latestUpdate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
