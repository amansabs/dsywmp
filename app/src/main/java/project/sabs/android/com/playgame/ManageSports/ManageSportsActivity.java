package project.sabs.android.com.playgame.ManageSports;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import project.sabs.android.com.playgame.AddNewEvent.AddNewEventActivitty;
import project.sabs.android.com.playgame.ManageUser.ManageUserActivity;
import project.sabs.android.com.playgame.ManageUser.UserListAdapter;
import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;
import project.sabs.android.com.playgame.SportsDetails.SportsBatchTimingListAdapter;
import project.sabs.android.com.playgame.SportsDetails.SportsCoachListAdapter;

public class ManageSportsActivity extends AppCompatActivity {

    RelativeLayout rr_back;
    TextView text_toolbar;

    RelativeLayout Img_Addcategory;
    RelativeLayout Img_AddBatchTime;
    RelativeLayout Img_Addcoach;

    RecyclerView recyclerview_sportsCategory;
    RecyclerView recyclerview_batchTime;
    RecyclerView recyclerview_sportsCoach;

    RelativeLayout rr_sportscategory;
    RelativeLayout rr_batchTime;
    RelativeLayout rr_sportsoach;

    ArrayList<CategoryModel>categoryModelArrayList = new ArrayList<CategoryModel>();
    SportsCategoryListAdapter  sportsCategoryListAdapter;

    /* ArrayList<CategoryModel>categoryModelArrayList = new ArrayList<CategoryModel>();
     ArrayList<CategoryModel>categoryModelArrayList = new ArrayList<CategoryModel>();*/
    private ManageSportsBatchTimingListAdapter sportsBatchTimingListAdapter;
    public ArrayList<BatchTimeModel> BatchList =new ArrayList<BatchTimeModel>();
    String startTime = "";
    String endTime =""   ;

    private MangeSportsCoachListAdapter mAdapter;
    public ArrayList<StudentModel> StudentList =new ArrayList<StudentModel>();


    EditText et_gameType;
    MangeGameListAdapter mangeGameListAdapter;
    public ArrayList<GameTypeModel> gameTypeAList =new ArrayList<GameTypeModel>();

    CircleImageView mImageView_edit;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private Bitmap certi_bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    ImageView user_profile_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_sports);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        findViews();

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        onClick();


        //Category
        sportsCategoryListAdapter = new SportsCategoryListAdapter(ManageSportsActivity.this, categoryModelArrayList,rr_sportscategory);
        recyclerview_sportsCategory.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageSportsActivity.this);
        recyclerview_sportsCategory.setLayoutManager(layoutManager);
        recyclerview_sportsCategory.setNestedScrollingEnabled(false);
        recyclerview_sportsCategory.setAdapter(sportsCategoryListAdapter);

        //Timing
        sportsBatchTimingListAdapter = new ManageSportsBatchTimingListAdapter(ManageSportsActivity.this, BatchList,rr_batchTime);
        recyclerview_batchTime.setHasFixedSize(true);
        GridLayoutManager GlayoutManager = new GridLayoutManager(ManageSportsActivity.this,2);
        recyclerview_batchTime.setLayoutManager(GlayoutManager);
        recyclerview_batchTime.setNestedScrollingEnabled(false);
        recyclerview_batchTime.setAdapter(sportsBatchTimingListAdapter);


        //Coach
        mAdapter = new MangeSportsCoachListAdapter(ManageSportsActivity.this, StudentList,rr_sportsoach);
        recyclerview_sportsCoach.setHasFixedSize(true);
        LinearLayoutManager slayoutManager = new LinearLayoutManager(ManageSportsActivity.this);
        recyclerview_sportsCoach.setLayoutManager(slayoutManager);
        recyclerview_sportsCoach.setNestedScrollingEnabled(false);
        recyclerview_sportsCoach.setAdapter(mAdapter);


        gameTypeAList.clear();
        gameTypeAList.add(new GameTypeModel("Individual"));
        gameTypeAList.add(new GameTypeModel("Double"));
        gameTypeAList.add(new GameTypeModel("Team"));
    }

    private void onClick() {

        et_gameType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageSportsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.game_typedialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Button add_button=dialog.findViewById(R.id.add_button);
                final RecyclerView recyclerview_gameType=dialog.findViewById(R.id.recyclerview_gameType);

                mangeGameListAdapter = new MangeGameListAdapter(ManageSportsActivity.this, gameTypeAList);
                recyclerview_gameType.setHasFixedSize(true);
                LinearLayoutManager slayoutManager = new LinearLayoutManager(ManageSportsActivity.this);
                recyclerview_gameType.setLayoutManager(slayoutManager);
                recyclerview_gameType.setNestedScrollingEnabled(false);
                recyclerview_gameType.setAdapter(mangeGameListAdapter);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                add_button.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!gameTypeAList.get(0).isIsclick() && !gameTypeAList.get(1).isIsclick() && !gameTypeAList.get(2).isIsclick()){

                            Toast.makeText(ManageSportsActivity.this, "Please select game type", Toast.LENGTH_SHORT).show();
                        }

                        else{
                            String gType = "";
                            boolean isFrist = true;
                            for (int i = 0; i < gameTypeAList.size(); i++) {

                                if (isFrist){
                                    if (gameTypeAList.get(i).isIsclick()){
                                        isFrist = false;
                                        gType = gameTypeAList.get(i).getGameType();
                                    }
                                }
                                else {

                                    if (gameTypeAList.get(i).isIsclick()){
                                        gType = gType+"," +gameTypeAList.get(i).getGameType();
                                    }
                                }
                            }
                            et_gameType.setText(gType);
                            dialog.cancel();
                        }
                    }
                });


            }
        });




        Img_Addcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageSportsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.add_sports_category_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Button add_button=dialog.findViewById(R.id.add_button);
                RelativeLayout rr_Category=dialog.findViewById(R.id.rr_Category);
                rr_Category.setVisibility(View.VISIBLE);
                final EditText et_ctFordialog=dialog.findViewById(R.id.et_ctFordialog);






                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                add_button.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String spCategory = et_ctFordialog.getText().toString();

                        if (spCategory.equalsIgnoreCase("")){

                            Toast.makeText(ManageSportsActivity.this, "Please enter category", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            categoryModelArrayList.add(new CategoryModel(spCategory));
                            addCategoryy();
                            dialog.cancel();
                        }

                    }
                });
            }
        });

        Img_AddBatchTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageSportsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.add_sports_batch_dialog);
                Button cancel_delete=dialog.findViewById(R.id.cancel_delete);
                RelativeLayout rr_cancel=dialog.findViewById(R.id.rr_cancel);
                Button add_button=dialog.findViewById(R.id.add_button);
                RelativeLayout rr_Category=dialog.findViewById(R.id.rr_Category);
                RelativeLayout rr_time=dialog.findViewById(R.id.rr_time);
                TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                txt_heading.setText("Batch Time");
                rr_Category.setVisibility(View.GONE);
                rr_time.setVisibility(View.VISIBLE);
                cancel_delete.setVisibility(View.INVISIBLE);

                final EditText et_BatchStartTiming=dialog.findViewById(R.id.et_BatchStartTiming);

                final EditText et_BatchEndTiming=dialog.findViewById(R.id.et_BatchEndTiming);
                final EditText et_PlayerNo=dialog.findViewById(R.id.et_PlayerNo);

                et_BatchStartTiming.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(ManageSportsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                if(selectedHour < 12) {
                                    startTime = "AM";
                                } else {
                                    startTime = "PM";
                                }
                                et_BatchStartTiming.setText( selectedHour + ":" + selectedMinute+" "+startTime);
                            }
                        }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });

                et_BatchEndTiming.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(ManageSportsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                if(selectedHour < 12) {
                                    endTime = "AM";
                                } else {
                                    endTime = "PM";
                                }
                                et_BatchEndTiming.setText( selectedHour + ":" + selectedMinute+" "+endTime);
                            }
                        }, hour, minute, true);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });


                rr_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();

                    }

                });

                add_button.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String bStartTime = et_BatchStartTiming.getText().toString();
                        String bEndTime = et_BatchEndTiming.getText().toString();
                        String playerNo = et_PlayerNo.getText().toString();


                        if (bStartTime.equalsIgnoreCase("")){

                            Toast.makeText(ManageSportsActivity.this, "Please enter start time", Toast.LENGTH_SHORT).show();
                        } else if (bEndTime.equalsIgnoreCase("")){

                            Toast.makeText(ManageSportsActivity.this, "Please enter end time", Toast.LENGTH_SHORT).show();
                        }else if (playerNo.equalsIgnoreCase("")){

                            Toast.makeText(ManageSportsActivity.this, "Please enter player number", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            BatchList.add(new BatchTimeModel(bStartTime,bEndTime,playerNo));
                            addBatch();
                            dialog.cancel();
                        }

                    }
                });


            }
        });




        Img_Addcoach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageSportsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.add_mangesports_coach_dialog);
                Button cancel_delete=dialog.findViewById(R.id.cancel_delete);
                Button add_button=dialog.findViewById(R.id.add_button);
                RelativeLayout rr_cancel=dialog.findViewById(R.id.rr_cancel);
                TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                final EditText et_CoachName=dialog.findViewById(R.id.et_CoachName);
                txt_heading.setText("Add Coach");
                cancel_delete.setVisibility(View.INVISIBLE);



                rr_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();

                    }

                });

                add_button.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String coachName = et_CoachName.getText().toString();

                        if (coachName.equalsIgnoreCase("")){

                            Toast.makeText(ManageSportsActivity.this, "Please enter start time", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            StudentList.add(new StudentModel(coachName,"7698989521","TT01523","Cricket","10 AM to 12 PM"
                                    ,"12 Nov to 30Dec 2019",R.drawable.user_one));
                            addCoach();
                            dialog.cancel();

                        }

                    }
                });

            }
        });

        mImageView_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage( );

            }
        });
    }

    private void findViews() {
        et_gameType = (EditText) findViewById(R.id.et_gameType);
        recyclerview_sportsCoach = (RecyclerView) findViewById(R.id.recyclerview_sportsCoach);
        recyclerview_batchTime = (RecyclerView) findViewById(R.id.recyclerview_batchTime);
        recyclerview_sportsCategory = (RecyclerView) findViewById(R.id.recyclerview_sportsCategory);
        rr_sportsoach = (RelativeLayout) findViewById(R.id.rr_sportsoach);
        rr_batchTime = (RelativeLayout) findViewById(R.id.rr_batchTime);
        rr_sportscategory = (RelativeLayout) findViewById(R.id.rr_sportscategory);
        Img_AddBatchTime = (RelativeLayout) findViewById(R.id.Img_AddBatchTime);
        Img_Addcategory = (RelativeLayout) findViewById(R.id.Img_Addcategory);
        Img_Addcoach = (RelativeLayout) findViewById(R.id.Img_Addcoach);
        rr_back = (RelativeLayout) findViewById(R.id.rr_back);
        text_toolbar = (TextView) findViewById(R.id.text_toolbar);
        mImageView_edit = (CircleImageView) findViewById(R.id.mImageView_edit);
        user_profile_image = (ImageView) findViewById(R.id.user_profile_image);
        text_toolbar.setText("Add New Sport");
    }

    private void addCategoryy(){

        if (categoryModelArrayList.size()>0){

            sportsCategoryListAdapter.notifyDataSetChanged();
            rr_sportscategory.setVisibility(View.VISIBLE);

        }

    }
    private void addBatch(){

        if (BatchList.size()>0){

            sportsBatchTimingListAdapter.notifyDataSetChanged();
            rr_batchTime.setVisibility(View.VISIBLE);

        }

    } private void  addCoach(){

        if (StudentList.size()>0){

            mAdapter.notifyDataSetChanged();
            rr_sportsoach.setVisibility(View.VISIBLE);

        }

    }

    // Select image from camera and gallery
    @SuppressLint("NewApi")
    private void selectImage( ) {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;

        if (requestCode == PICK_IMAGE_CAMERA) {


            if (data!= null){

                try {

                    Uri selectedImage = data.getData();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Camera::>>> ");
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    user_profile_image.setImageBitmap(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data!= null){
                Uri selectedImage = data.getData();
                try {
                    Bitmap  bitmap = scaleImage(this,selectedImage);
                    // Bitmap  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Gallery::>>> ");
                    user_profile_image.setImageBitmap(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                //  Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 1200 || rotatedHeight > 1200) {
            float widthRatio = ((float) rotatedWidth) / ((float) 1200);
            float heightRatio = ((float) rotatedHeight) / ((float) 1200);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

}
