package project.sabs.android.com.playgame.ManagePlayer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManageAchievements.ManageAchievementsActivity;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class GameListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<GameNameModel> modelList;
    public static GameListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;
    Dialog dialog;
    EditText et_PlayerGame;
    String category = "";


    public GameListAdapter(Context context, ArrayList<GameNameModel> modelList, Dialog dialog, EditText et_PlayerGame,String category) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.dialog = dialog;
        this.et_PlayerGame = et_PlayerGame;
        this.category = category;


    }




    public void updateList(ArrayList<GameNameModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.game_list_item, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final GameNameModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;



            genericViewHolder.gameName.setText(model.getGameName());

            genericViewHolder.rr_GameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();

                    /*if (category.equalsIgnoreCase("category")){
                        et_category.setText(model.getGameName());
                    }*/
                    et_PlayerGame.setText(model.getGameName());
                }
            });

            //setAnimation(holder.itemView, position);

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private GameNameModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, GameListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView gameName;
        private RelativeLayout rr_GameLayout;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.gameName = (TextView) itemView.findViewById(R.id.gameName);
            this.rr_GameLayout = (RelativeLayout) itemView.findViewById(R.id.rr_GameLayout);


            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

