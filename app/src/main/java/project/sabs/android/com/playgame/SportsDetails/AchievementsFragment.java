package project.sabs.android.com.playgame.SportsDetails;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;


public class AchievementsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    private SportsAchievementListAdapter mAdapter;
    public ArrayList<SportsAchievementModel> achievementModelArrayList =new ArrayList<SportsAchievementModel>();
    RecyclerView recycler_achievement_list;

    public AchievementsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AchievementsFragment newInstance(String param1, String param2) {
        AchievementsFragment fragment = new AchievementsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_achievements, container, false);
        recycler_achievement_list   = (RecyclerView) rootView.findViewById(R.id.recycler_achievement_list);
        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            /*Toast.makeText(getContext(), "setuservisible", Toast.LENGTH_SHORT).show();
            callBack();*/
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){

            AchievementList();
        }
    }
    private void AchievementList()
    {
        achievementModelArrayList.clear();

        achievementModelArrayList.add(new SportsAchievementModel("Aman Rai","1 Bronze, 7th International Junior Shotgun Cup, 2015 Orimattila (Finland)",
                "1 Bronze, ISSF Junior World Cup (Ri e/Pistol), 2016 Gabala, Azerbaijan",
                "1 Silver, 1 Bronze, 59 National Shooting Championship, 2015 New Delhi (India)"));

        achievementModelArrayList.add(new SportsAchievementModel("Rahul Jain","1 Bronze, ISSF Junior World Cup (Ri e/Pistol), 2016 Gabala, Azerbaijan",
                "",
                ""));

        achievementModelArrayList.add(new SportsAchievementModel("Sachin Sharma","1 Silver, 1 Bronze, 59 National Shooting Championship, 2015 New Delhi (India)",
                "",
                ""));



        mAdapter = new SportsAchievementListAdapter(getContext(), achievementModelArrayList);
        recycler_achievement_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_achievement_list.setLayoutManager(layoutManager);
        recycler_achievement_list.setAdapter(mAdapter);


    }

}
