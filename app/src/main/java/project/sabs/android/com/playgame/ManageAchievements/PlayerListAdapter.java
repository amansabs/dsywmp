package project.sabs.android.com.playgame.ManageAchievements;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class PlayerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<StudentModel> modelList;
    private ArrayList<StudentModel> arraylist;
    public static PlayerListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public PlayerListAdapter(Context context, ArrayList<StudentModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.arraylist = new ArrayList<StudentModel>();
        this.arraylist.addAll(modelList);


    }


    public void updateList(ArrayList<StudentModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_player_adaptor_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final StudentModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.UserimageView.setImageResource(model.getUserImage());

            genericViewHolder.Txtplayer_name.setText(model.getSName());

            genericViewHolder.Txtplayer_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

              /*  Toast.makeText(mContext, ""+ErrorCodeLookupActivity.
                        movieNamesArrayList.get(position).getFilename(), Toast.LENGTH_SHORT).show();*/

                    ManageAchievementsActivity.recycler_view_player_list.setVisibility(View.GONE);
                    String file_name=model.getSName();
                    ManageAchievementsActivity.searchView.setQuery(file_name,false);
                    ManageAchievementsActivity.recycler_view_player_list.setVisibility(View.GONE);

                }
            });

          //  setAnimation(holder.itemView, position);

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        modelList.clear();
        if (charText.length() == 0) {
            modelList.addAll(arraylist);
        } else {
            for (StudentModel wp : arraylist) {
                if (wp.getSName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    modelList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }



    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private StudentModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, PlayerListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView UserimageView;
        private TextView Txtplayer_name;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.UserimageView = (ImageView) itemView.findViewById(R.id.UserimageView);
            this.Txtplayer_name = (TextView) itemView.findViewById(R.id.Txtplayer_name);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

