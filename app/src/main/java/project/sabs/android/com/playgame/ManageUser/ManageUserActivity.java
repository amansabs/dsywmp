package project.sabs.android.com.playgame.ManageUser;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;

public class ManageUserActivity extends AppCompatActivity {
    RelativeLayout rr_back;
    TextView text_toolbar;
    ArrayList<UserModel>userModelArrayList = new ArrayList<UserModel>();
    UserListAdapter userListAdapter;
    RecyclerView userlistRecyclerview;
    RelativeLayout rr_noRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_user);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        findViews();

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        UserList();
    }

    private void findViews() {

        rr_noRecord = (RelativeLayout) findViewById(R.id.rr_noRecord);
        rr_back = (RelativeLayout) findViewById(R.id.rr_back);
        text_toolbar = (TextView) findViewById(R.id.text_toolbar);
        text_toolbar.setText("Users");
        userlistRecyclerview = (RecyclerView) findViewById(R.id.userlistRecyclerview);


    }
    private void UserList()
    {
        userModelArrayList.clear();

        userModelArrayList.add(new UserModel("Rahul Jain","rahul.12@gmail.com","Cricket",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Aman Rai","aman.12@gmail.com","Badminton",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Subham Jain","shubham.12@gmail.com","Athletics ",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Rajat Jain","rajat.12@gmail.com","Archery",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Rahul Jain","rahul.12@gmail.com","Shooting",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Aman Rai","aman.12@gmail.com","Swimming",R.drawable.user_one));
        userModelArrayList.add(new UserModel("Subham Jain","shubham.12@gmail.com","Women’s Hockey ",R.drawable.user_one));


    if (userModelArrayList.size()>0){
        userListAdapter = new UserListAdapter(ManageUserActivity.this, userModelArrayList,rr_noRecord,userlistRecyclerview);
        userlistRecyclerview.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageUserActivity.this);
        userlistRecyclerview.setLayoutManager(layoutManager);
        userlistRecyclerview.setAdapter(userListAdapter);

    }
    else {
        rr_noRecord.setVisibility(View.VISIBLE);
        userlistRecyclerview.setVisibility(View.GONE);
    }








    }
}
