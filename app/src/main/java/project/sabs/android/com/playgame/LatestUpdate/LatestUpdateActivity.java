package project.sabs.android.com.playgame.LatestUpdate;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import project.sabs.android.com.playgame.ChangePassword.LatestUpdateListAdapter;
import project.sabs.android.com.playgame.ChangePassword.LatestUpdateModel;
import project.sabs.android.com.playgame.Player.StudentListAdapter;
import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;

public class LatestUpdateActivity extends AppCompatActivity {
    RelativeLayout rr_back;
    TextView text_toolbar;
    public ArrayList<LatestUpdateModel> latestUpdateModelArrayList =new ArrayList<LatestUpdateModel>();
    RecyclerView recycler_view_ladestUpdate;
    LatestUpdateListAdapter latestUpdateListAdapter;
    LinearLayoutManager layoutManager;
    final int time = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latest_update);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        recycler_view_ladestUpdate = (RecyclerView)findViewById(R.id.recycler_view_ladestUpdate);

        rr_back = (RelativeLayout)findViewById(R.id.rr_back);

        text_toolbar = (TextView)findViewById(R.id.text_toolbar);

        text_toolbar.setText("Latest Update");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        LatestUpdate();



    }

    private void LatestUpdate()
    {
        latestUpdateModelArrayList.clear();

        latestUpdateModelArrayList.add(new LatestUpdateModel("Madhya Pradesh State Shooting Academy athlete Manisha Keer shoots gold once again at Khelo India Youth Games-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Three athletes– Aishwary Pratap, Sunidhi Chouhan, and Chinky Yadav from MP State Shooting Academy will represent India at the ISSF world cup.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Central Secretariat becomes champion in Men’s category and RSB Rajasthan in women’s at All India Civil Services Kabaddi Tournament-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("3 little girls from Tatya Tope Sports Complex will participate in National Billiards Snooker Tournament in Indore.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("MP’s medal count should be 4 times more at the next Khelo India Youth Games-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Central Secretariat becomes champion in Men’s category and RSB Rajasthan in women’s at All India Civil Services Kabaddi Tournament-2019.","August 20, 2019"  ));

        latestUpdateModelArrayList.add(new LatestUpdateModel("Madhya Pradesh State Shooting Academy athlete Manisha Keer shoots gold once again at Khelo India Youth Games-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Three athletes– Aishwary Pratap, Sunidhi Chouhan, and Chinky Yadav from MP State Shooting Academy will represent India at the ISSF world cup.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Central Secretariat becomes champion in Men’s category and RSB Rajasthan in women’s at All India Civil Services Kabaddi Tournament-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("3 little girls from Tatya Tope Sports Complex will participate in National Billiards Snooker Tournament in Indore.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("MP’s medal count should be 4 times more at the next Khelo India Youth Games-2019.","August 19, 2019"  ));
        latestUpdateModelArrayList.add(new LatestUpdateModel("Central Secretariat becomes champion in Men’s category and RSB Rajasthan in women’s at All India Civil Services Kabaddi Tournament-2019.","August 20, 2019"  ));


        latestUpdateListAdapter = new LatestUpdateListAdapter(LatestUpdateActivity.this, latestUpdateModelArrayList);
        recycler_view_ladestUpdate.setHasFixedSize(true);
         layoutManager = new LinearLayoutManager(LatestUpdateActivity.this);
        recycler_view_ladestUpdate.setLayoutManager(layoutManager);
        recycler_view_ladestUpdate.setAdapter(latestUpdateListAdapter);


    }

}
