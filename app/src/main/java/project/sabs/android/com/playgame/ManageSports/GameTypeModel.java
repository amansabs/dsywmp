package project.sabs.android.com.playgame.ManageSports;

public class GameTypeModel {

    String GameType;

   boolean isclick =false;

    public boolean isIsclick() {
        return isclick;
    }

    public void setIsclick(boolean isclick) {
        this.isclick = isclick;
    }

    public String getGameType() {
        return GameType;
    }

    public void setGameType(String gameType) {
        GameType = gameType;
    }

    public GameTypeModel(String gameType) {

        GameType = gameType;
    }
}
