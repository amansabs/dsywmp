package project.sabs.android.com.playgame.Register;

public class TimingModel {

    String id;
    String Timing;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTiming() {
        return Timing;
    }

    public void setTiming(String timing) {
        Timing = timing;
    }

    public TimingModel(String id, String timing) {

        this.id = id;
        Timing = timing;
    }
    @Override
    public String toString() {
        return getTiming();
    }
}
