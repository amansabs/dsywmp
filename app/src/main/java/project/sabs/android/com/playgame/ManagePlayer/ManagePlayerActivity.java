package project.sabs.android.com.playgame.ManagePlayer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.hdodenhof.circleimageview.CircleImageView;
import project.sabs.android.com.playgame.AddNewEvent.AddNewEventActivitty;
import project.sabs.android.com.playgame.Login.LoginActivity;
import project.sabs.android.com.playgame.MainActivity;
import project.sabs.android.com.playgame.R;
import project.sabs.android.com.playgame.SportsDetails.SportsBatchTimingListAdapter;

public class ManagePlayerActivity extends AppCompatActivity {
    RelativeLayout rr_back;
    TextView text_toolbar;

    private Bitmap certi_bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private String CropID = "";
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    CircleImageView user_profile_image;
    CircleImageView mImageView_edit;
    EditText et_PlayerGame;
    EditText et_BatchTiming;
    EditText et_FromDate;
    EditText et_endDate;
    RecyclerView Game_recycler;
    ArrayList<GameNameModel>gameNameModelArrayList = new ArrayList<GameNameModel>();
    GameListAdapter gameListAdapter;

    PlayerBatchTimingListAdapter playerBatchTimingListAdapter;
    public ArrayList<String> BatchList =new ArrayList<String>();
    LinearLayout layoutManager;

    int mYear,mMonth,mDay;
    //

    RelativeLayout rr_uploadMarksheeet;
    RelativeLayout uploadMakrseetImage;
    RelativeLayout rr_uploadid;
    RelativeLayout rr_uploadIdView;
    ImageView img_marksheet;
    ImageView img_id;


    String  key = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_player);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        rr_uploadIdView = (RelativeLayout)findViewById(R.id.rr_uploadIdView);
        rr_uploadid = (RelativeLayout)findViewById(R.id.rr_uploadid);
        rr_uploadMarksheeet = (RelativeLayout)findViewById(R.id.rr_uploadMarksheeet);
        uploadMakrseetImage = (RelativeLayout)findViewById(R.id.uploadMakrseetImage);
        rr_back = (RelativeLayout)findViewById(R.id.rr_back);
        user_profile_image = (CircleImageView) findViewById(R.id.user_profile_image);
        mImageView_edit = (CircleImageView) findViewById(R.id.mImageView_edit);
        et_PlayerGame = (EditText) findViewById(R.id.et_PlayerGame);
        et_BatchTiming = (EditText) findViewById(R.id.et_BatchTiming);
        et_FromDate = (EditText) findViewById(R.id.et_FromDate);
        et_endDate = (EditText) findViewById(R.id.et_endDate);

        text_toolbar = (TextView)findViewById(R.id.text_toolbar);
        img_marksheet = (ImageView) findViewById(R.id.img_marksheet);
        img_id = (ImageView) findViewById(R.id.img_id);

        text_toolbar.setText("Add New Player");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mImageView_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                key = "";
                selectImage();

            }
        });

        et_PlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManagePlayerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.game_list_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Game_recycler=dialog.findViewById(R.id.Game_recycler);

                gameList(dialog);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });
        et_BatchTiming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManagePlayerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.game_list_dialog);
                TextView dialog_titel = dialog.findViewById(R.id.dialog_titel);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Game_recycler=dialog.findViewById(R.id.Game_recycler);
                dialog_titel.setText("Select Batch Time");


                BatchList(dialog);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });


        et_FromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                // To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(ManagePlayerActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        String myFormat = "dd/MM/yy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);
                        et_FromDate.setText(sdf.format(myCalendar.getTime()));

                        mDay = selectedday;
                        mMonth = selectedmonth;
                        mYear = selectedyear;
                    }
                }, mYear, mMonth, mDay);
                //mDatePicker.setTitle("Select date");
                mDatePicker.show();


            }
        });

        et_endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                // To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(ManagePlayerActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        String myFormat = "dd/MM/yy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);
                        et_endDate.setText(sdf.format(myCalendar.getTime()));

                        mDay = selectedday;
                        mMonth = selectedmonth;
                        mYear = selectedyear;
                    }
                }, mYear, mMonth, mDay);
                //mDatePicker.setTitle("Select date");
                mDatePicker.show();

            }
        });

        rr_uploadMarksheeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                key = "Marksheet";
                selectImage();


            }
        });

        rr_uploadIdView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                key = "ID";
                selectImage();

            }
        });

    }


    private void BatchList(Dialog dialog)
    {
        BatchList.clear();
        BatchList.add("8 Am - 9 AM");
        BatchList.add("9 Am - 9:30 AM");
        BatchList.add("10:30 Am - 11 AM");
        BatchList.add("2 PM - 3 PM");
        BatchList.add("4:30 PM - 6 PM"); BatchList.add("8 Am - 9 AM");
        BatchList.add("9 Am - 9:30 AM");
        BatchList.add("10:30 Am - 11 AM");
        BatchList.add("2 PM - 3 PM");
        BatchList.add("4:30 PM - 6 PM");

        playerBatchTimingListAdapter = new PlayerBatchTimingListAdapter(ManagePlayerActivity.this, BatchList,dialog,et_BatchTiming);
        Game_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManagePlayerActivity.this);
        Game_recycler.setLayoutManager(layoutManager);
        Game_recycler.setAdapter(playerBatchTimingListAdapter);


    }
    private void gameList(Dialog dialog){
        gameNameModelArrayList.clear();
        gameNameModelArrayList.add(new GameNameModel("1","Cricket"));
        gameNameModelArrayList.add(new GameNameModel("2","Hockey"));
        gameNameModelArrayList.add(new GameNameModel("3","Footbal"));
        gameNameModelArrayList.add(new GameNameModel("4","Shooting"));
        gameNameModelArrayList.add(new GameNameModel("4","Chess"));
        gameNameModelArrayList.add(new GameNameModel("4","Athelet"));

        gameListAdapter = new GameListAdapter(ManagePlayerActivity.this, gameNameModelArrayList,dialog,et_PlayerGame,"");
        Game_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManagePlayerActivity.this);
        Game_recycler.setLayoutManager(layoutManager);
        Game_recycler.setAdapter(gameListAdapter);
        
    }

    // Select image from camera and gallery
    @SuppressLint("NewApi")
    private void selectImage( ) {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;

        if (requestCode == PICK_IMAGE_CAMERA) {


            if (data!= null){

                try {

                    Uri selectedImage = data.getData();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Camera::>>> ");
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (key.equalsIgnoreCase("Marksheet")){
                        uploadMakrseetImage.setVisibility(View.VISIBLE);
                        img_marksheet.setImageBitmap(bitmap);
                        key = "";
                    }

                  else   if (key.equalsIgnoreCase("ID")){

                        img_id.setImageBitmap(bitmap);
                        rr_uploadid.setVisibility(View.VISIBLE);
                        key = "";
                    }
                    else {

                        user_profile_image.setImageBitmap(bitmap);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data!= null){


                Uri selectedImage = data.getData();
                try {
                      Bitmap  bitmap = scaleImage(this,selectedImage);
                   // Bitmap  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Gallery::>>> ");

                    if (key.equalsIgnoreCase("Marksheet")){
                        uploadMakrseetImage.setVisibility(View.VISIBLE);
                        img_marksheet.setImageBitmap(bitmap);
                        key = "";
                    }
                    else   if (key.equalsIgnoreCase("ID")){

                        img_id.setImageBitmap(bitmap);
                        rr_uploadid.setVisibility(View.VISIBLE);
                        key = "";
                    }

                    else {

                        user_profile_image.setImageBitmap(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                //  Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 1200 || rotatedHeight > 450) {
            float widthRatio = ((float) rotatedWidth) / ((float) 1200);
            float heightRatio = ((float) rotatedHeight) / ((float) 450);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

}
