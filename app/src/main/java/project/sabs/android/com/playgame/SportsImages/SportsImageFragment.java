package project.sabs.android.com.playgame.SportsImages;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Player.PlayerListFragment;
import project.sabs.android.com.playgame.R;
import project.sabs.android.com.playgame.SportsDetails.SportsDetailHomeFragment;


public class SportsImageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View rootView;
    RecyclerView recycler_sports_list;
    public ArrayList<SportImageModel> SportsList =new ArrayList<SportImageModel>();
    private SportsGalleryListAdapter mAdapter;
    GridLayoutManager gridlayoutManager;



    // TODO: Rename and change types and number of parameters
    public static SportsImageFragment newInstance(String param1, String param2) {
        SportsImageFragment fragment = new SportsImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sports_image, container, false);
        recycler_sports_list= rootView.findViewById(R.id.recycler_sports_list);

        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created

            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){
           /* getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                        // handle back button

                        return true;

                    }

                    return false;
                }
            });*/
            StudentList();
        }
    }
    private void StudentList()
    {
        SportsList.clear();
        SportsList.add(new SportImageModel(R.drawable.img_one));
        SportsList.add(new SportImageModel(R.drawable.img_two));
        SportsList.add(new SportImageModel(R.drawable.img_three));
        SportsList.add(new SportImageModel(R.drawable.img_four));
        SportsList.add(new SportImageModel(R.drawable.img_five));
        SportsList.add(new SportImageModel(R.drawable.img_six));
        SportsList.add(new SportImageModel(R.drawable.img_seven));
        SportsList.add(new SportImageModel(R.drawable.img_eight));
        SportsList.add(new SportImageModel(R.drawable.img_nine));
        SportsList.add(new SportImageModel(R.drawable.img_ten));
        SportsList.add(new SportImageModel(R.drawable.img_eleven));
        SportsList.add(new SportImageModel(R.drawable.img_twelve));
        SportsList.add(new SportImageModel(R.drawable.img_thirteen));
        SportsList.add(new SportImageModel(R.drawable.img_fourteen));
        SportsList.add(new SportImageModel(R.drawable.img_fifteen));
        mAdapter = new SportsGalleryListAdapter(getContext(), SportsList);

        recycler_sports_list.setHasFixedSize(true);
        gridlayoutManager = new GridLayoutManager(getContext(), 2);
        recycler_sports_list.setLayoutManager(gridlayoutManager);
        recycler_sports_list.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new SportsGalleryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, SportImageModel model) {

                //handle item click events here
                SportsDetailHomeFragment nextFrag= new SportsDetailHomeFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, nextFrag);
                fragmentTransaction.addToBackStack("SportsImageFragment");
                fragmentTransaction.commit();

            }

            @Override
            public void onLongClicked(View itemView, final int position, final SportImageModel model) {

                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.logout_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Button logout_btn=dialog.findViewById(R.id.logout_button);
                TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                TextView txt_subject=dialog.findViewById(R.id.txt_subject);
                txt_heading.setText("Delete Player");
                logout_btn.setText("DELETE");
                txt_subject.setText("Are you sure you want to delete this sport ?");

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });



                logout_btn.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SportsList.remove(position);
                        mAdapter.notifyDataSetChanged();
                        dialog.dismiss();

                    }
                });

            }
        });





    }

}
