package project.sabs.android.com.playgame.ManageAchievements;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManagePlayer.GameListAdapter;
import project.sabs.android.com.playgame.ManagePlayer.GameNameModel;
import project.sabs.android.com.playgame.ManagePlayer.ManagePlayerActivity;
import project.sabs.android.com.playgame.ManageSports.CategoryModel;
import project.sabs.android.com.playgame.ManageSports.ManageSportsActivity;
import project.sabs.android.com.playgame.ManageSports.SportsCategoryListAdapter;
import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;
import project.sabs.android.com.playgame.SportsDetails.SportsCoachListAdapter;

public class ManageAchievementsActivity extends AppCompatActivity  implements SearchView.OnQueryTextListener{
    RelativeLayout rr_back;
    TextView text_toolbar;
    EditText et_PlayerGame;
    EditText et_PlayerName;
    EditText et_category;
    RecyclerView Game_recycler;
    RecyclerView recyclerview_addAchievements;
    ArrayList<GameNameModel> gameNameModelArrayList = new ArrayList<GameNameModel>();
    GameListAdapter gameListAdapter;
    public static RecyclerView recycler_view_player_list;
    public  static  SearchView searchView;
    public ArrayList<StudentModel> StudentList =new ArrayList<StudentModel>();
    private PlayerListAdapter mAdapter;

    RelativeLayout Img_Addcategory;
    RelativeLayout rr_sportscategory;
    ArrayList<CategoryModel>categoryModelArrayList = new ArrayList<CategoryModel>();
    AddAchievementsListAdapter addAchievementsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_achievements);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        findViews();

        rr_back = (RelativeLayout)findViewById(R.id.rr_back);

        text_toolbar = (TextView)findViewById(R.id.text_toolbar);

        text_toolbar.setText("Add Achievements");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        clickView();

        StudentList();
        searchView.setOnQueryTextListener(this);

        //Category
        addAchievementsListAdapter = new AddAchievementsListAdapter(ManageAchievementsActivity.this, categoryModelArrayList,rr_sportscategory);
        recyclerview_addAchievements.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageAchievementsActivity.this);
        recyclerview_addAchievements.setLayoutManager(layoutManager);
        recyclerview_addAchievements.setNestedScrollingEnabled(false);
        recyclerview_addAchievements.setAdapter(addAchievementsListAdapter);


    }

    private void findViews() {

        et_category = (EditText) findViewById(R.id.et_category);
        et_PlayerGame = (EditText) findViewById(R.id.et_PlayerGame);
        et_PlayerName = (EditText) findViewById(R.id.et_PlayerName);
        rr_sportscategory = (RelativeLayout) findViewById(R.id.rr_sportscategory);
        recycler_view_player_list= (RecyclerView) findViewById(R.id.recycler_view_player_list);
        recyclerview_addAchievements= (RecyclerView) findViewById(R.id.recyclerview_addAchievements);
        searchView = (SearchView) findViewById(R.id.searchPlayer);
        Img_Addcategory = (RelativeLayout) findViewById(R.id.Img_Addcategory);
        recycler_view_player_list.setVisibility(View.GONE);

    }  private void clickView() {

        et_PlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageAchievementsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.game_list_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Game_recycler=dialog.findViewById(R.id.Game_recycler);

                gameList(dialog);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

        et_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageAchievementsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.game_list_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Game_recycler=dialog.findViewById(R.id.Game_recycler);

                gameCAtegory( dialog);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

        Img_Addcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ManageAchievementsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.show();
                dialog.setContentView(R.layout.add_sports_category_dialog);
                Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                Button add_button=dialog.findViewById(R.id.add_button);
                TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                TextView txt_sportsName=dialog.findViewById(R.id.txt_sportsName);
                txt_heading.setText("Add Achievements");
                txt_sportsName.setText("Achievements");
                RelativeLayout rr_Category=dialog.findViewById(R.id.rr_Category);
                rr_Category.setVisibility(View.VISIBLE);
                final EditText et_ctFordialog=dialog.findViewById(R.id.et_ctFordialog);
                et_ctFordialog.setHint("Enter Achievement");





                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                add_button.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String spCategory = et_ctFordialog.getText().toString();

                        if (spCategory.equalsIgnoreCase("")){

                            Toast.makeText(ManageAchievementsActivity.this, "Please enter category", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            categoryModelArrayList.add(new CategoryModel(spCategory));
                            addAchievements();
                            dialog.cancel();
                        }

                    }
                });
            }
        });

    }

    private void gameList(Dialog dialog){


        gameNameModelArrayList.clear();
        gameNameModelArrayList.add(new GameNameModel("1","Cricket"));
        gameNameModelArrayList.add(new GameNameModel("2","Hockey"));
        gameNameModelArrayList.add(new GameNameModel("3","Footbal"));
        gameNameModelArrayList.add(new GameNameModel("4","Shooting"));
        gameNameModelArrayList.add(new GameNameModel("4","Chess"));
        gameNameModelArrayList.add(new GameNameModel("4","Athelet"));

        gameListAdapter = new GameListAdapter(ManageAchievementsActivity.this, gameNameModelArrayList,dialog,et_PlayerGame,"");
        Game_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageAchievementsActivity.this);
        Game_recycler.setLayoutManager(layoutManager);
        Game_recycler.setAdapter(gameListAdapter);

    }

    private void gameCAtegory(Dialog dialog){


        gameNameModelArrayList.clear();
        gameNameModelArrayList.add(new GameNameModel("1","100 Meter"));
        gameNameModelArrayList.add(new GameNameModel("2","200 Meter"));
        gameNameModelArrayList.add(new GameNameModel("3","250 Meter"));
        gameNameModelArrayList.add(new GameNameModel("4","300 Meter"));
        gameNameModelArrayList.add(new GameNameModel("4","350 Meter"));


        gameListAdapter = new GameListAdapter(ManageAchievementsActivity.this, gameNameModelArrayList,dialog,et_category, "Category");
        Game_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageAchievementsActivity.this);
        Game_recycler.setLayoutManager(layoutManager);
        Game_recycler.setAdapter(gameListAdapter);

    }

    private void StudentList()
    {
        StudentList.clear();

        StudentList.add(new StudentModel("Rahul Jain","7698989521","TT01523","Cricket","10 AM to 12 PM"
                ,"12 Nov to 30Dec 2019",R.drawable.user_one));
        StudentList.add(new StudentModel("Sachin Verma","7987673022","TT01522","Athlet","08 AM to 10 AM"
                ,"12 Oct to 12 Jan 2020",R.drawable.three_img));


        mAdapter = new PlayerListAdapter(ManageAchievementsActivity.this, StudentList);
        recycler_view_player_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageAchievementsActivity.this);
        recycler_view_player_list.setLayoutManager(layoutManager);
        recycler_view_player_list.setAdapter(mAdapter);


    }
    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        recycler_view_player_list.setVisibility(View.GONE);
        if (newText.length()>0)
        {
            recycler_view_player_list.setVisibility(View.VISIBLE);
        }

        else if (newText.isEmpty())
        {
            recycler_view_player_list.setVisibility(View.GONE);
        }
        else {
            recycler_view_player_list.setVisibility(View.GONE);
        }


        String text = newText;
        mAdapter.filter(text);
        return false;
    }

    private void addAchievements(){

        if (categoryModelArrayList.size()>0){

            addAchievementsListAdapter.notifyDataSetChanged();
            rr_sportscategory.setVisibility(View.VISIBLE);

        }

    }

}
