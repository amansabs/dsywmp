package project.sabs.android.com.playgame.ManagePlayer;

public class GameNameModel {
    String gameID;
    String gameName;

    public GameNameModel(String gameID, String gameName) {
        this.gameID = gameID;
        this.gameName = gameName;
    }

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
