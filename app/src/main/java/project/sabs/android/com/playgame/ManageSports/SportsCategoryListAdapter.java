package project.sabs.android.com.playgame.ManageSports;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SportsCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<CategoryModel> modelList;
    public static SportsCategoryListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    RelativeLayout rr_sportscategory;
    private OnItemClickListener mItemClickListener;


    public SportsCategoryListAdapter(Context context, ArrayList<CategoryModel> modelList, RelativeLayout rr_sportscategory) {
        this.mContext = context;
        this.modelList = modelList;
        this.rr_sportscategory = rr_sportscategory;

    }


    public void updateList(ArrayList<CategoryModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_category_item_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final CategoryModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            int  num = 1 + position;

            genericViewHolder.categoryTxt.setText(num+"-"+model.getCategoryName());

           genericViewHolder.rr_categoryClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.edit_sports_category_dialog);
                    Button cancel_delete=dialog.findViewById(R.id.cancel_delete);
                    Button add_update=dialog.findViewById(R.id.add_update);
                    RelativeLayout rr_cancel=dialog.findViewById(R.id.rr_cancel);
                    RelativeLayout rr_Category=dialog.findViewById(R.id.rr_Category);
                    rr_Category.setVisibility(View.VISIBLE);
                    final EditText et_ctFordialog=dialog.findViewById(R.id.et_ctFordialog);

                    et_ctFordialog.setText(model.getCategoryName());


                    rr_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });


                    cancel_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialog.dismiss();
                            modelList.remove(position);
                            notifyDataSetChanged();
                            if(modelList.size()<=0){

                                rr_sportscategory.setVisibility(View.GONE);
                            }
                        }
                    });

                    add_update.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            String category = et_ctFordialog.getText().toString();
                            if (category.equalsIgnoreCase("")){
                                Toast.makeText(mContext, "Please enter category ", Toast.LENGTH_SHORT).show();
                            }

                            else {

                                modelList.get(position).setCategoryName(category);
                                notifyDataSetChanged();
                                dialog.dismiss();

                            }

                        }
                    });

                }
            });


            setAnimation(holder.itemView, position);

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private CategoryModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SportsCategoryListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView categoryTxt;
        private RelativeLayout rr_categoryClick;


        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);


            this.categoryTxt = (TextView) itemView.findViewById(R.id.categoryTxt);
            this.rr_categoryClick = (RelativeLayout) itemView.findViewById(R.id.rr_categoryClick);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

