package project.sabs.android.com.playgame.Home;

public class Directorate {
    String post;
    String name;
    String about;
    int img;

    public Directorate(String post, String name, String about, int img) {
        this.post = post;
        this.name = name;
        this.about = about;
        this.img = img;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
