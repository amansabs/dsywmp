package project.sabs.android.com.playgame.ManageSports;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class MangeGameListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<GameTypeModel> modelList;
    public static MangeGameListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    RelativeLayout rr_sportsoach;
    private OnItemClickListener mItemClickListener;


    public MangeGameListAdapter(Context context, ArrayList<GameTypeModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;

    }


    public void updateList(ArrayList<GameTypeModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.game_type_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final GameTypeModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            if (modelList.get(position).isIsclick()){
                genericViewHolder.cb_gameTpye.setChecked(true);
            }

            genericViewHolder.cb_gameTpye.setText(model.getGameType());

            genericViewHolder.cb_gameTpye.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                    if (isChecked){

                        modelList.get(position).setIsclick(true);
                    }
                    else {

                        modelList.get(position).setIsclick(false);

                    }

                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private GameTypeModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, MangeGameListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

     
        private CheckBox cb_gameTpye;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.cb_gameTpye = (CheckBox) itemView.findViewById(R.id.cb_gameTpye);
          


        }

    }

}

