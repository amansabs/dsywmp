package project.sabs.android.com.playgame.Events;

public class EventModel {

    String GameName;
    String Date;
    String City;
    String Place;
    String GameType;
    int Image;

    public EventModel(String gameName, String date, String city, String place, String gameType, int image) {
        GameName = gameName;
        Date = date;
        City = city;
        Place = place;
        GameType = gameType;
        Image = image;
    }

    public String getGameName() {
        return GameName;
    }

    public void setGameName(String gameName) {
        GameName = gameName;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getGameType() {
        return GameType;
    }

    public void setGameType(String gameType) {
        GameType = gameType;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }
}
