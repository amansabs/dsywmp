package project.sabs.android.com.playgame.ManageSports;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class ManageSportsBatchTimingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<BatchTimeModel> modelList;
    public static ManageSportsBatchTimingListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    BatchTimeModel gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    String startTime ="";
    String endTime ="";

    private OnItemClickListener mItemClickListener;
    RelativeLayout rr_batchTime;

    public ManageSportsBatchTimingListAdapter(Context context, ArrayList<BatchTimeModel> modelList, RelativeLayout rr_batchTime) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.rr_batchTime = rr_batchTime;


    }


    public void updateList(ArrayList<BatchTimeModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_batch_timing_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final BatchTimeModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;
            genericViewHolder.txt_timing.setText(modelList.get(position).getStartTime()+"-"+modelList.get(position).getEndTime());
            genericViewHolder.rr_coach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.add_sports_batch_dialog);
                    Button cancel_delete=dialog.findViewById(R.id.cancel_delete);
                    RelativeLayout rr_cancel=dialog.findViewById(R.id.rr_cancel);
                    Button add_button=dialog.findViewById(R.id.add_button);
                    RelativeLayout rr_Category=dialog.findViewById(R.id.rr_Category);
                    RelativeLayout rr_time=dialog.findViewById(R.id.rr_time);
                    TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                    txt_heading.setText("Batch Time");
                    rr_Category.setVisibility(View.GONE);
                    rr_time.setVisibility(View.VISIBLE);
                    cancel_delete.setVisibility(View.VISIBLE);
                    add_button.setText("UPDATE");
                    final EditText et_BatchStartTiming=dialog.findViewById(R.id.et_BatchStartTiming);
                    final EditText et_BatchEndTiming=dialog.findViewById(R.id.et_BatchEndTiming);
                    final EditText et_PlayerNo=dialog.findViewById(R.id.et_PlayerNo);
                    et_BatchStartTiming.setText(model.getStartTime());
                    et_BatchEndTiming.setText(model.getEndTime());
                    et_PlayerNo.setText(model.getPlayerNumber());


                    et_BatchStartTiming.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            // TODO Auto-generated method stub
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    if(selectedHour < 12) {
                                        startTime = "AM";
                                    } else {
                                        startTime = "PM";
                                    }
                                    et_BatchStartTiming.setText( selectedHour + ":" + selectedMinute+" "+startTime);
                                }
                            }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();

                        }
                    });

                    et_BatchEndTiming.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            // TODO Auto-generated method stub
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    if(selectedHour < 12) {
                                        endTime = "AM";
                                    } else {
                                        endTime = "PM";
                                    }
                                    et_BatchEndTiming.setText( selectedHour + ":" + selectedMinute+" "+endTime);
                                }
                            }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();

                        }
                    });


                    rr_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialog.dismiss();

                        }

                    }); cancel_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            modelList.remove(position);
                            notifyDataSetChanged();
                            dialog.dismiss();
                            if (modelList.size()<=0){
                                rr_batchTime.setVisibility(View.GONE);

                            }

                        }

                    });

                    add_button.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            String bStartTime = et_BatchStartTiming.getText().toString();
                            String bEndTime = et_BatchEndTiming.getText().toString();
                            String playerNo = et_PlayerNo.getText().toString();


                            if (bStartTime.equalsIgnoreCase("")){

                                Toast.makeText(mContext, "Please enter start time", Toast.LENGTH_SHORT).show();
                            } else if (bEndTime.equalsIgnoreCase("")){

                                Toast.makeText(mContext, "Please enter end time", Toast.LENGTH_SHORT).show();
                            }else if (playerNo.equalsIgnoreCase("")){

                                Toast.makeText(mContext, "Please enter player number", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {

                                dialog.cancel();
                                modelList.get(position).setStartTime(bStartTime);
                                modelList.get(position).setEndTime(bEndTime);
                                modelList.get(position).setPlayerNumber(playerNo);
                                notifyDataSetChanged();

                            }

                        }
                    });

                }
            });


        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private BatchTimeModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, ManageSportsBatchTimingListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView txt_timing;
        private RelativeLayout rr_coach;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);


            this.txt_timing = (TextView) itemView.findViewById(R.id.txt_timing);
            this.rr_coach = (RelativeLayout) itemView.findViewById(R.id.rr_coach);


        }

    }

}

