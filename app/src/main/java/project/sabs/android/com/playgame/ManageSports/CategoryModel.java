package project.sabs.android.com.playgame.ManageSports;

public class CategoryModel {
    String CategoryName;

    public CategoryModel(String categoryName) {
        CategoryName = categoryName;
    }


    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }
}
