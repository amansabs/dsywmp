package project.sabs.android.com.playgame.Events;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import project.sabs.android.com.playgame.MainActivity;
import project.sabs.android.com.playgame.Player.PlayerListFragment;
import project.sabs.android.com.playgame.R;

public class EventHomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;


    public EventHomeFragment() {
        // Required empty public constructor
    }

    View rootview;
    private TabLayout tabLayout;
    private ViewPager EventviewPager;

    public static EventHomeFragment newInstance(String param1, String param2) {
        EventHomeFragment fragment = new EventHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootview = inflater.inflate(R.layout.fragment_event_home, container, false);

        EventviewPager = (ViewPager) rootview.findViewById(R.id.Eventviewpager);
        tabLayout = (TabLayout) rootview.findViewById(R.id.event_tabs);
        setupViewPager(EventviewPager);
        tabLayout.setupWithViewPager(EventviewPager);

        return rootview;

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            /*Toast.makeText(getContext(), "setuservisible", Toast.LENGTH_SHORT).show();
            callBack();*/
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){


        }
    }

    private void setupViewPager(ViewPager viewPager) {
      ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new EventListFragment(), "UPCOMING");
        adapter.addFragment(new EventListFragment(), "RECENT");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
