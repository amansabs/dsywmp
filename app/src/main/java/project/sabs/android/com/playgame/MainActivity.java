package project.sabs.android.com.playgame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import project.sabs.android.com.playgame.AboutUs.AboutUsActivity;
import project.sabs.android.com.playgame.AddNewEvent.AddNewEventActivitty;
import project.sabs.android.com.playgame.AddNewUser.AddNewUserActivity;
import project.sabs.android.com.playgame.ChangePassword.ChangePasswordActivity;
import project.sabs.android.com.playgame.ContactUs.ContactUsActivity;
import project.sabs.android.com.playgame.Events.EventHomeFragment;
import project.sabs.android.com.playgame.Events.EventListFragment;
import project.sabs.android.com.playgame.Home.HomeFragment;
import project.sabs.android.com.playgame.LatestUpdate.LatestUpdateActivity;
import project.sabs.android.com.playgame.Login.LoginActivity;
import project.sabs.android.com.playgame.ManageAchievements.ManageAchievementsActivity;
import project.sabs.android.com.playgame.ManagePlayer.ManagePlayerActivity;
import project.sabs.android.com.playgame.ManageSports.ManageSportsActivity;
import project.sabs.android.com.playgame.ManageUser.ManageUserActivity;
import project.sabs.android.com.playgame.MyProfile.MyProfileActivity;
import project.sabs.android.com.playgame.Player.PlayerListFragment;
import project.sabs.android.com.playgame.SportsImages.SportsImageFragment;
import project.sabs.android.com.playgame.SportsImages.SportsImageViewActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private int[] tabIcons = {R.drawable.home_ic,R.drawable.events_ic, R.drawable.player_ic,R.drawable.sports_ic};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();
    }
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);


        tabLayout.getTabAt(0).getIcon().setColorFilter(Color.parseColor("#FF4081"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(Color.parseColor("#23448D"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(Color.parseColor("#23448D"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).getIcon().setColorFilter(Color.parseColor("#23448D"), PorterDuff.Mode.SRC_IN);




        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#FF4081"), PorterDuff.Mode.SRC_IN);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#23448D"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_latest_update) {

            Intent intent =new Intent(MainActivity.this,LatestUpdateActivity.class);
            startActivity(intent);

            return true;
        }else if (id == R.id.action_contact) {
            Intent intent =new Intent(MainActivity.this,ContactUsActivity.class);
            startActivity(intent);


            return true;
        } else if (id == R.id.action_aboutUs) {

            Intent intent =new Intent(MainActivity.this,AboutUsActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_AddNewUser) {

            Intent intent =new Intent(MainActivity.this,AddNewUserActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_ManageUser) {

            Intent in = new Intent(MainActivity.this,ManageUserActivity.class);
            startActivity(in);
            // Handle the camera action
        } else if (id == R.id.nav_ManageEvents) {
            Intent intent =new Intent(MainActivity.this,AddNewEventActivitty.class);
            startActivity(intent);

        }else if (id == R.id.nav_ManagePlayers) {

            Intent intent =new Intent(MainActivity.this,ManagePlayerActivity.class);
            startActivity(intent);


        }
        else if (id == R.id.nav_ManageSports) {

            Intent intent =new Intent(MainActivity.this,ManageSportsActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_ManageAchievements  ) {

            Intent intent =new Intent(MainActivity.this,ManageAchievementsActivity.class);
            startActivity(intent);
        }/*else if (id == R.id.nav_Gallery) {


        }*/ else if (id == R.id.nav_account) {
            Intent intent =new Intent(MainActivity.this,MyProfileActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_changePass) {

            Intent intent =new Intent(MainActivity.this,ChangePasswordActivity.class);
            startActivity(intent);

        }  else if (id == R.id.nav_Logout) {

            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
            dialog.setContentView(R.layout.logout_dialog);
            Button cancel_btn=dialog.findViewById(R.id.cancel_button);
            Button logout_btn=dialog.findViewById(R.id.logout_button);
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            logout_btn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent =new Intent(MainActivity.this,LoginActivity.class);
                    startActivity(intent);
                    dialog.dismiss();
                    finish();

                }
            });

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new HomeFragment(), "HOME");
        adapter.addFragment(new EventHomeFragment(), "EVENTS");
        adapter.addFragment(new PlayerListFragment(), "PLAYERS");
        adapter.addFragment(new SportsImageFragment(), "SPORTS");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
