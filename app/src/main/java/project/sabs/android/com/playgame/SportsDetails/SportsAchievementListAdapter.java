package project.sabs.android.com.playgame.SportsDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.ManageAchievements.ManageAchievementsActivity;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SportsAchievementListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<SportsAchievementModel> modelList;
    public static SportsAchievementListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public SportsAchievementListAdapter(Context context, ArrayList<SportsAchievementModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<SportsAchievementModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_achievement_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final SportsAchievementModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            //   genericViewHolder.CoachimageView.setImageResource(model.getUserImage());

            genericViewHolder.txtachievements.setText(model.getAchievement1());
            genericViewHolder.winnerName.setText(model.getName());
            genericViewHolder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, ManageAchievementsActivity.class);
                    mContext.startActivity(in);

                }
            });

            /* +"\n"+model.getAchievement2()+"\n"+model.getAchievement3()*/

            genericViewHolder.rr_coach.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {


                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.logout_dialog);
                    Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                    Button logout_btn=dialog.findViewById(R.id.logout_button);
                    TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                    TextView txt_subject=dialog.findViewById(R.id.txt_subject);
                    txt_heading.setText("Delete Player");
                    logout_btn.setText("DELETE");
                    txt_subject.setText("Are you sure you want to delete this sport ?");

                    cancel_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });



                    logout_btn.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            modelList.remove(position);
                            notifyDataSetChanged();
                            dialog.dismiss();

                        }
                    });


                    return true;
                }
            });





        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private SportsAchievementModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SportsAchievementListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView CoachimageView;
        private ImageView img_edit;
        private TextView txtachievements;
        private TextView winnerName;
        private LinearLayout rr_coach;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.CoachimageView = (ImageView) itemView.findViewById(R.id.CoachimageView);
            this.img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            this.txtachievements = (TextView) itemView.findViewById(R.id.txtachievements);
            this.winnerName = (TextView) itemView.findViewById(R.id.winnerName);
            this.rr_coach = (LinearLayout) itemView.findViewById(R.id.rr_coach);


        }

    }

}

