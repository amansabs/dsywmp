package project.sabs.android.com.playgame.Events;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import project.sabs.android.com.playgame.AddNewEvent.AddNewEventActivitty;
import project.sabs.android.com.playgame.Login.LoginActivity;
import project.sabs.android.com.playgame.MainActivity;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class EventsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<EventModel> modelList;
    public static EventsListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public EventsListAdapter(Context context, ArrayList<EventModel> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<EventModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_select_crop_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final EventModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.img_Game.setImageResource(model.getImage());
            genericViewHolder.txt_eventDate.setText(model.getDate());
            genericViewHolder.txt_cityName.setText(model.getCity());
            genericViewHolder.Txtgame_name.setText(model.getGameName());
            genericViewHolder.Txtgame_typeName.setText(model.getGameType());
            genericViewHolder.TxtplaceName.setText(model.getPlace());


            genericViewHolder.rr_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(mContext,EventDetailActivity.class);
                    mContext.startActivity(in);
                }
            });


             genericViewHolder.rr_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(mContext,AddNewEventActivitty.class);
                    mContext.startActivity(in);
                }
            });

             genericViewHolder.rr_product.setOnLongClickListener(new View.OnLongClickListener() {

                 @Override
                 public boolean onLongClick(View view) {


                     final Dialog dialog = new Dialog(mContext);
                     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                     dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                     dialog.setCancelable(false);
                     dialog.show();
                     dialog.setContentView(R.layout.logout_dialog);
                     Button cancel_btn=dialog.findViewById(R.id.cancel_button);
                     Button logout_btn=dialog.findViewById(R.id.logout_button);
                     TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                     TextView txt_subject=dialog.findViewById(R.id.txt_subject);
                     txt_heading.setText("Delete Event");
                     logout_btn.setText("DELETE");
                     txt_subject.setText("Are you sure you want to delete this event ?");
                     cancel_btn.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View view) {
                             dialog.dismiss();
                         }
                     });



                     logout_btn.setOnClickListener( new View.OnClickListener() {
                         @Override
                         public void onClick(View view) {

                             modelList.remove(position);
                             notifyDataSetChanged();
                             dialog.dismiss();

                         }
                     });






                     return true;
                 }

             });


            //setAnimation(holder.itemView, position);

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private EventModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, EventsListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_Game;
        private TextView txt_eventDate;
        private TextView txt_cityName;
        private TextView Txtgame_name;
        private TextView Txtgame_typeName;
        private TextView TxtplaceName;
        private RelativeLayout rr_product;
        private RelativeLayout rr_edit;

        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_Game = (ImageView) itemView.findViewById(R.id.img_Game);
            this.txt_eventDate = (TextView) itemView.findViewById(R.id.txt_eventDate);
            this.txt_cityName = (TextView) itemView.findViewById(R.id.txt_cityName);
            this.Txtgame_name = (TextView) itemView.findViewById(R.id.Txtgame_name);
            this.Txtgame_typeName = (TextView) itemView.findViewById(R.id.Txtgame_typeName);
            this.TxtplaceName = (TextView) itemView.findViewById(R.id.TxtplaceName);
            this.rr_product = (RelativeLayout) itemView.findViewById(R.id.rr_product);
            this.rr_edit = (RelativeLayout) itemView.findViewById(R.id.rr_edit);



            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

