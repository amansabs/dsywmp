package project.sabs.android.com.playgame.ManageSports;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class MangeSportsCoachListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<StudentModel> modelList;
    public static MangeSportsCoachListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    RelativeLayout rr_sportsoach;
    private OnItemClickListener mItemClickListener;


    public MangeSportsCoachListAdapter(Context context, ArrayList<StudentModel> modelList, RelativeLayout rr_sportsoach) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.rr_sportsoach = rr_sportsoach;


    }


    public void updateList(ArrayList<StudentModel> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_coach_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final StudentModel model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.CoachimageView.setImageResource(model.getUserImage());

            genericViewHolder.txtCoachName.setText(model.getSName());
            genericViewHolder.rr_coach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.setContentView(R.layout.add_mangesports_coach_dialog);
                    Button cancel_delete=dialog.findViewById(R.id.cancel_delete);
                    Button add_button=dialog.findViewById(R.id.add_button);
                    RelativeLayout rr_cancel=dialog.findViewById(R.id.rr_cancel);
                    TextView txt_heading=dialog.findViewById(R.id.txt_heading);
                    final EditText et_CoachName=dialog.findViewById(R.id.et_CoachName);
                    txt_heading.setText("Add Coach");
                    cancel_delete.setVisibility(View.VISIBLE);
                    add_button.setText("UPDATE");

                    et_CoachName.setText(model.getSName());

                    rr_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialog.dismiss();

                        }

                    });
                    cancel_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            modelList.remove(position);
                            dialog.dismiss();
                            notifyDataSetChanged();

                            if (modelList.size()<=0){
                                rr_sportsoach.setVisibility(View.GONE);
                            }

                        }

                    });

                    add_button.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            String coachName = et_CoachName.getText().toString();

                            if (coachName.equalsIgnoreCase("")){

                                Toast.makeText(mContext, "Please enter start time", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                dialog.cancel();
                                modelList.get(position).setSName(coachName);
                                notifyDataSetChanged();
                            }

                        }
                    });

                }
            });

        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private StudentModel getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, MangeSportsCoachListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView CoachimageView;
        private TextView txtCoachName;
        private RelativeLayout rr_coach;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.CoachimageView = (ImageView) itemView.findViewById(R.id.CoachimageView);
            this.txtCoachName = (TextView) itemView.findViewById(R.id.txtCoachName);
            this.rr_coach = (RelativeLayout) itemView.findViewById(R.id.rr_coach);


        }

    }

}

