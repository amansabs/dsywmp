package project.sabs.android.com.playgame.ManageSports;

public class BatchTimeModel {
    String startTime;
    String endTime;
    String playerNumber;

    public BatchTimeModel(String startTime, String endTime, String playerNumber) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.playerNumber = playerNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }
}
