package project.sabs.android.com.playgame.SportsDetails;

public class SportsAchievementModel {

    String Name;
    String Achievement1;
    String Achievement2;
    String Achievement3;

    public SportsAchievementModel(String name, String achievement1, String achievement2, String achievement3) {
        Name = name;
        Achievement1 = achievement1;
        Achievement2 = achievement2;
        Achievement3 = achievement3;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAchievement1() {
        return Achievement1;
    }

    public void setAchievement1(String achievement1) {
        Achievement1 = achievement1;
    }

    public String getAchievement2() {
        return Achievement2;
    }

    public void setAchievement2(String achievement2) {
        Achievement2 = achievement2;
    }

    public String getAchievement3() {
        return Achievement3;
    }

    public void setAchievement3(String achievement3) {
        Achievement3 = achievement3;
    }
}
