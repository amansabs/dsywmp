package project.sabs.android.com.playgame.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import me.relex.circleindicator.CircleIndicator;
import project.sabs.android.com.playgame.ChangePassword.LatestUpdateListAdapter;
import project.sabs.android.com.playgame.ChangePassword.LatestUpdateModel;
import project.sabs.android.com.playgame.LatestUpdate.LatestUpdateActivity;
import project.sabs.android.com.playgame.R;

public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public HomeFragment() {
        // Required empty public constructor
    }

    View rootView;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private static final Integer[] IMAGES= {R.drawable.merathon_img,R.drawable.shooting_img,R.drawable.hocky_img,R.drawable.merathon_img};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    public ArrayList<Directorate> directorateArrayList =new ArrayList<Directorate>();
    RecyclerView recycler_view_directoret;
    DirectorateListAdapter directorateListAdapter;
    LinearLayoutManager layoutManager;

    public ArrayList<SocialSite> socialSiteArrayList =new ArrayList<SocialSite>();
    RecyclerView recycler_view_social;
    SocialSiteListAdapter socialSiteListAdapter;

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_home, container, false);
        recycler_view_directoret = (RecyclerView)rootView.findViewById(R.id.recycler_view_directoret);
        recycler_view_social = (RecyclerView)rootView.findViewById(R.id.recycler_view_social);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created

            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()) {
            init();
            LatestUpdate();
            socialSite();
        }
    }


    private void init() {
        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(getContext(),ImagesArray));


        final CircleIndicator indicator = (CircleIndicator)
                rootView.findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;
        //Set circle indicator radius
        //indicator.setre(5 * density);

        NUM_PAGES =IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler() {
            @Override
            public void publish(LogRecord logRecord) {

            }

            @Override
            public void flush() {

            }

            @Override
            public void close() throws SecurityException {

            }
        };
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();

        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                indicator.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
    private void LatestUpdate()
    {
        directorateArrayList.clear();

        directorateArrayList.add(new Directorate("Minister","Mr. Jitu Patwari","Hon’ble Minister, Sports and Youth Welfare, Madhya Pradesh",R.drawable.jitu_img));
        directorateArrayList.add(new Directorate("Additional Chief Secretary","Mrs. Veera Rana, IAS","Additional Chief Secretary, Sports and Youth Welfare, Madhya Pradesh",R.drawable.veera_img));
        directorateArrayList.add(new Directorate("Directorate","Dr. S. L. Thaosen, IPS","Director, Sports and Youth Welfare, Madhya Pradesh",R.drawable.thason));
        directorateArrayList.add(new Directorate("Directorate","Dr. Vinod Pradhan","Joint Director, Sports and Youth Welfare, Madhya Pradesh",R.drawable.pradhan_img));
        directorateArrayList.add(new Directorate("Directorate","Mr. B. S. Yadav","Joint Director, Sports and Youth Welfare, Madhya Pradesh",R.drawable.yadav_img));
        //directorateArrayList.add(new Directorate("Directorate","Mr. B. S. Yadav","Joint Director, Sports and Youth Welfare, Madhya Pradesh",R.drawable.yadav_img));
        directorateArrayList.add(new Directorate("Directorate","Ms. Alpna Ojha","Deputy Director Finance, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Mr. Jose Chako","Deputy Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Mr. Jalaj Chaturvedi","Assistant Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Mr. O P Harod","Assistant Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Mr. Vikas Kharadkar","Assistant Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Dr. Shipra Srivastava","Assistant Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Dr. Krishna Kant Khare","Assistant Director, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Mr. Pradeep Rawat","Sports officer, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateArrayList.add(new Directorate("Directorate","Ms. Uma Patel","Youth Welfare Officer, Sports and Youth Welfare, Madhya Pradesh",0));
        directorateListAdapter = new DirectorateListAdapter(getContext(), directorateArrayList);
        recycler_view_directoret.setHasFixedSize(true);
         final GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                if (position == 4){
                    return 2;
                }
                else {
                    return 1;
                }
            }
        });

        recycler_view_directoret.setLayoutManager(layoutManager);
        recycler_view_directoret.setNestedScrollingEnabled(false);
        recycler_view_directoret.setAdapter(directorateListAdapter);


    }  private void socialSite()
    {
        socialSiteArrayList.clear();

        socialSiteArrayList.add(new SocialSite("Facebook",R.drawable.fb_ic));
        socialSiteArrayList.add(new SocialSite("Instagram",R.drawable.insta_ic));
        socialSiteArrayList.add(new SocialSite("Twitter",R.drawable.twitter_ic));

        socialSiteListAdapter = new SocialSiteListAdapter(getContext(), socialSiteArrayList);
        GridLayoutManager layoutManager1 = new GridLayoutManager(getContext(),3);
        recycler_view_social.setHasFixedSize(true);
        recycler_view_social.setNestedScrollingEnabled(false);
        recycler_view_social.setLayoutManager(layoutManager1);
        recycler_view_social.setAdapter(socialSiteListAdapter);


    }

}
