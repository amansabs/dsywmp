package project.sabs.android.com.playgame.Home;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class DirectorateListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<Directorate> modelList;
    public static DirectorateListAdapter objSearchListAdapter;
    // private ArrayList<EventsListAdapter> addToCartModelList =new ArrayList<EventsListAdapter>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public DirectorateListAdapter(Context context, ArrayList<Directorate> modelList) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;


    }


    public void updateList(ArrayList<Directorate> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_directorate_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final Directorate model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;


            if (model.getImg() == 0){
                genericViewHolder.image_person.setVisibility(View.GONE);

                genericViewHolder.image_person.setImageResource(model.getImg());
                genericViewHolder.txt_designation.setText(model.getPost());
                genericViewHolder.txt_person_name.setText(model.getName());
                genericViewHolder.txt_about.setText(model.getAbout());
            }
            else {
                genericViewHolder.image_person.setVisibility(View.VISIBLE);
                genericViewHolder.image_person.setImageResource(model.getImg());
                genericViewHolder.txt_designation.setText(model.getPost());
                genericViewHolder.txt_person_name.setText(model.getName());
                genericViewHolder.txt_about.setText(model.getAbout());

            }




        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Directorate getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, DirectorateListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView image_person;
        private TextView txt_designation;
        private TextView txt_person_name;
        private TextView txt_about;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);


            this.image_person = (ImageView) itemView.findViewById(R.id.image_person);
            this.txt_designation = (TextView) itemView.findViewById(R.id.txt_designation);
            this.txt_person_name = (TextView) itemView.findViewById(R.id.txt_person_name);
            this.txt_about = (TextView) itemView.findViewById(R.id.txt_about);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

