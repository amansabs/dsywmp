package project.sabs.android.com.playgame.ManageUser;

public class UserModel {
    String uName;
    String uMobile;
    String uDepartment;
    int uImage;


    public UserModel(String uName, String uMobile, String uDepartment, int uImage) {
        this.uName = uName;
        this.uMobile = uMobile;
        this.uDepartment = uDepartment;
        this.uImage = uImage;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuMobile() {
        return uMobile;
    }

    public void setuMobile(String uMobile) {
        this.uMobile = uMobile;
    }

    public String getuDepartment() {
        return uDepartment;
    }

    public void setuDepartment(String uDepartment) {
        this.uDepartment = uDepartment;
    }

    public int getuImage() {
        return uImage;
    }

    public void setuImage(int uImage) {
        this.uImage = uImage;
    }
}
