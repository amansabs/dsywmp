package project.sabs.android.com.playgame.Player;

public class StudentModel {

    String SName;
    String SMobile;
    String SENROLL;
    String SGame;
    String STiming;
    String SMemberShip;
    int UserImage;

    public String getSName() {
        return SName;
    }

    public void setSName(String SName) {
        this.SName = SName;
    }

    public String getSMobile() {
        return SMobile;
    }

    public void setSMobile(String SMobile) {
        this.SMobile = SMobile;
    }

    public String getSENROLL() {
        return SENROLL;
    }

    public void setSENROLL(String SENROLL) {
        this.SENROLL = SENROLL;
    }

    public String getSGame() {
        return SGame;
    }

    public void setSGame(String SGame) {
        this.SGame = SGame;
    }

    public String getSTiming() {
        return STiming;
    }

    public void setSTiming(String STiming) {
        this.STiming = STiming;
    }

    public String getSMemberShip() {
        return SMemberShip;
    }

    public void setSMemberShip(String SMemberShip) {
        this.SMemberShip = SMemberShip;
    }

    public int getUserImage() {
        return UserImage;
    }

    public void setUserImage(int userImage) {
        UserImage = userImage;
    }

    public StudentModel(String SName, String SMobile, String SENROLL, String SGame, String STiming, String SMemberShip, int userImage) {

        this.SName = SName;
        this.SMobile = SMobile;
        this.SENROLL = SENROLL;
        this.SGame = SGame;
        this.STiming = STiming;
        this.SMemberShip = SMemberShip;
        UserImage = userImage;
    }
}
