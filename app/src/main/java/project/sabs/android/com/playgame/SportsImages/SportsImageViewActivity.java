package project.sabs.android.com.playgame.SportsImages;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Player.StudentModel;
import project.sabs.android.com.playgame.R;

public class SportsImageViewActivity extends AppCompatActivity {

    RecyclerView recycler_sports_list;
    public ArrayList<SportImageModel> SportsList =new ArrayList<SportImageModel>();
    private SportsGalleryListAdapter mAdapter;
    GridLayoutManager gridlayoutManager;
    RelativeLayout rr_back;
    TextView text_toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_image_view);
        recycler_sports_list= (RecyclerView) findViewById(R.id.recycler_sports_list);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        rr_back = (RelativeLayout)findViewById(R.id.rr_back);
        text_toolbar = (TextView)findViewById(R.id.text_toolbar);

        text_toolbar.setText("Sports");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        StudentList();
    }


    private void StudentList()
    {
        SportsList.clear();
        SportsList.add(new SportImageModel(R.drawable.img_one));
        SportsList.add(new SportImageModel(R.drawable.img_two));
        SportsList.add(new SportImageModel(R.drawable.img_three));
        SportsList.add(new SportImageModel(R.drawable.img_four));
        SportsList.add(new SportImageModel(R.drawable.img_five));
        SportsList.add(new SportImageModel(R.drawable.img_six));
        SportsList.add(new SportImageModel(R.drawable.img_seven));
        SportsList.add(new SportImageModel(R.drawable.img_eight));
        SportsList.add(new SportImageModel(R.drawable.img_nine));
        SportsList.add(new SportImageModel(R.drawable.img_ten));
        SportsList.add(new SportImageModel(R.drawable.img_eleven));
        SportsList.add(new SportImageModel(R.drawable.img_twelve));
        SportsList.add(new SportImageModel(R.drawable.img_thirteen));
        SportsList.add(new SportImageModel(R.drawable.img_fourteen));
        SportsList.add(new SportImageModel(R.drawable.img_fifteen));



    mAdapter = new SportsGalleryListAdapter(SportsImageViewActivity.this, SportsList);
        recycler_sports_list.setHasFixedSize(true);
        gridlayoutManager = new GridLayoutManager(SportsImageViewActivity.this, 2);
        recycler_sports_list.setLayoutManager(gridlayoutManager);
        recycler_sports_list.setAdapter(mAdapter);


    }
}
