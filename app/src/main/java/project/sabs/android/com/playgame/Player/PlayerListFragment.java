package project.sabs.android.com.playgame.Player;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import project.sabs.android.com.playgame.Events.EventModel;
import project.sabs.android.com.playgame.Events.EventsListAdapter;
import project.sabs.android.com.playgame.R;

public class PlayerListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    private StudentListAdapter mAdapter;
    public ArrayList<StudentModel> StudentList =new ArrayList<StudentModel>();
    RecyclerView recycler_view_student_list;

    public PlayerListFragment() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parameters
    public static PlayerListFragment newInstance(String param1, String param2) {
        PlayerListFragment fragment = new PlayerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_player_list, container, false);
        recycler_view_student_list= (RecyclerView) rootView.findViewById(R.id.recycler_view_student_list);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            /*Toast.makeText(getContext(), "setuservisible", Toast.LENGTH_SHORT).show();
            callBack();*/
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getUserVisibleHint()){
            StudentList();
        }
    }
    private void StudentList()
    {
        StudentList.clear();

        StudentList.add(new StudentModel("Rahul Jain","7698989521","TT01523","Cricket","10 AM to 12 PM"
                ,"12 Nov to 30Dec 2019",R.drawable.user_one));
        StudentList.add(new StudentModel("Sachin Verma","7987673022","TT01522","Athlet","08 AM to 10 AM"
                ,"12 Oct to 12 Jan 2020",R.drawable.three_img));
        StudentList.add(new StudentModel("Anand Rajput","7698989500","TT01524","Cricket","10 AM to 12 PM"
                ,"12 Sep to 30 Dec 2019",R.drawable.user_one));
        StudentList.add(new StudentModel("Gaurav Gupta","7698989521","TT01523","Hocky","10 AM to 12 PM"
                ,"12 Sep to 30 Dec 2019",R.drawable.twoimg));
        StudentList.add(new StudentModel("Sachin Verma","7987673022","TT01522","Shooting","08 AM to 10 AM"
                ,"12 Nov to 30Dec 2019",R.drawable.four_img));
        StudentList.add(new StudentModel("Anand Rajput","7698989500","TT01524","Athlet","10 AM to 12 PM"
                ,"12 Nov to 30Dec 2019",R.drawable.three_img));



        mAdapter = new StudentListAdapter(getContext(), StudentList);
        recycler_view_student_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_student_list.setLayoutManager(layoutManager);
        recycler_view_student_list.setAdapter(mAdapter);


    }

}
